var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');

// Create SASS task
gulp.task('sass', function() {
	return gulp.src(['wp-content/themes/emmerson/scss/*.scss'])
	.pipe(sass())
	.on('error', function (err) {
        console.log(err.toString());
        this.emit('end');
    })
    .pipe(autoprefixer({
    	browsers: ['last 99 versions'],
    	cascade: false
    }))
	.pipe(gulp.dest("wp-content/themes/emmerson"));
});

// Move js into correct folder
gulp.task('js', function() {
	return gulp.src(['node_modules/bootstrap/dist/js/bootstrap.min.js', 'node_modules/jquery/dist/jquery.min.js', 'node_modules/popper.js/dist/popper.min.js', 'node_modules/owl.carousel/dist/owl.carousel.min.js', 'node_modules/modal-video/js/jquery-modal-video.min.js'])
	.pipe(gulp.dest("wp-content/themes/emmerson/js"));
});

// Build and watch
gulp.task('build', ['sass'], function() {

	gulp.watch(['wp-content/themes/emmerson/scss/*.scss'], ['sass']);

});

// Default task
gulp.task('default', ['js', 'build']);