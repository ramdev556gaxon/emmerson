<?php
/**
 * Register portfolio 
 */
function emmerson_portfolio() {

	$labels = array(
		'name'                  => _x( 'Portfolio', 'Post Type General Name', 'emmerson' ),
		'singular_name'         => _x( 'Portfolio', 'Post Type Singular Name', 'emmerson' ),
		'menu_name'             => __( 'Portfolio', 'emmerson' ),
		'name_admin_bar'        => __( 'Portfolio', 'emmerson' ),
		'archives'              => __( 'Item Archives', 'emmerson' ),
		'attributes'            => __( 'Item Attributes', 'emmerson' ),
		'parent_item_colon'     => __( 'Parent Item:', 'emmerson' ),
		'all_items'             => __( 'All Items', 'emmerson' ),
		'add_new_item'          => __( 'Add New Item', 'emmerson' ),
		'add_new'               => __( 'Add New', 'emmerson' ),
		'new_item'              => __( 'New Item', 'emmerson' ),
		'edit_item'             => __( 'Edit Item', 'emmerson' ),
		'update_item'           => __( 'Update Item', 'emmerson' ),
		'view_item'             => __( 'View Item', 'emmerson' ),
		'view_items'            => __( 'View Items', 'emmerson' ),
		'search_items'          => __( 'Search Item', 'emmerson' ),
		'not_found'             => __( 'Not found', 'emmerson' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'emmerson' ),
		'featured_image'        => __( 'Featured Image', 'emmerson' ),
		'set_featured_image'    => __( 'Set featured image', 'emmerson' ),
		'remove_featured_image' => __( 'Remove featured image', 'emmerson' ),
		'use_featured_image'    => __( 'Use as featured image', 'emmerson' ),
		'insert_into_item'      => __( 'Insert into item', 'emmerson' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'emmerson' ),
		'items_list'            => __( 'Items list', 'emmerson' ),
		'items_list_navigation' => __( 'Items list navigation', 'emmerson' ),
		'filter_items_list'     => __( 'Filter items list', 'emmerson' ),
	);
	$rewrite = array(
		'slug'                  => 'our-work',
		'with_front'            => true,
		'pages'                 => true,
		'feeds'                 => true,
	);
	$args = array(
		'label'                 => __( 'Portfolio', 'emmerson' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'revisions', 'custom-fields', 'page-attributes' ),
		'taxonomies'            => array( 'emmerson_portfolio_cat' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-portfolio',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => 'our-work',
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'rewrite'               => $rewrite,
		'capability_type'       => 'page',
	);
	register_post_type( 'emmerson_portfolio', $args );

}
add_action( 'init', 'emmerson_portfolio', 0 );

/**
 * Register portfolio category 
 */
function emmerson_portfolio_category() {

	$labels = array(
		'name'                       => _x( 'Categories', 'Taxonomy General Name', 'emmerson' ),
		'singular_name'              => _x( 'Category', 'Taxonomy Singular Name', 'emmerson' ),
		'menu_name'                  => __( 'Category', 'emmerson' ),
		'all_items'                  => __( 'All Items', 'emmerson' ),
		'parent_item'                => __( 'Parent Item', 'emmerson' ),
		'parent_item_colon'          => __( 'Parent Item:', 'emmerson' ),
		'new_item_name'              => __( 'New Item Name', 'emmerson' ),
		'add_new_item'               => __( 'Add New Item', 'emmerson' ),
		'edit_item'                  => __( 'Edit Item', 'emmerson' ),
		'update_item'                => __( 'Update Item', 'emmerson' ),
		'view_item'                  => __( 'View Item', 'emmerson' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'emmerson' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'emmerson' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'emmerson' ),
		'popular_items'              => __( 'Popular Items', 'emmerson' ),
		'search_items'               => __( 'Search Items', 'emmerson' ),
		'not_found'                  => __( 'Not Found', 'emmerson' ),
		'no_terms'                   => __( 'No items', 'emmerson' ),
		'items_list'                 => __( 'Items list', 'emmerson' ),
		'items_list_navigation'      => __( 'Items list navigation', 'emmerson' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => false,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => false,
		'query_var'                  => 'emmerson-portfolio-cat',
	);
	register_taxonomy( 'emmerson_portfolio_cat', array( 'emmerson_portfolio' ), $args );

}
add_action( 'init', 'emmerson_portfolio_category', 0 );

/**
 * Register Team Custom Post Type
 */
function emmerson_team() {

	$labels = array(
		'name'                  => _x( 'Teams', 'Post Type General Name', 'emmerson' ),
		'singular_name'         => _x( 'Teams', 'Post Type Singular Name', 'emmerson' ),
		'menu_name'             => __( 'Teams', 'emmerson' ),
		'name_admin_bar'        => __( 'Teams', 'emmerson' ),
		'archives'              => __( 'Item Archives', 'emmerson' ),
		'attributes'            => __( 'Item Attributes', 'emmerson' ),
		'parent_item_colon'     => __( 'Parent Item:', 'emmerson' ),
		'all_items'             => __( 'All Items', 'emmerson' ),
		'add_new_item'          => __( 'Add New Item', 'emmerson' ),
		'add_new'               => __( 'Add New', 'emmerson' ),
		'new_item'              => __( 'New Item', 'emmerson' ),
		'edit_item'             => __( 'Edit Item', 'emmerson' ),
		'update_item'           => __( 'Update Item', 'emmerson' ),
		'view_item'             => __( 'View Item', 'emmerson' ),
		'view_items'            => __( 'View Items', 'emmerson' ),
		'search_items'          => __( 'Search Item', 'emmerson' ),
		'not_found'             => __( 'Not found', 'emmerson' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'emmerson' ),
		'featured_image'        => __( 'Featured Image', 'emmerson' ),
		'set_featured_image'    => __( 'Set featured image', 'emmerson' ),
		'remove_featured_image' => __( 'Remove featured image', 'emmerson' ),
		'use_featured_image'    => __( 'Use as featured image', 'emmerson' ),
		'insert_into_item'      => __( 'Insert into item', 'emmerson' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'emmerson' ),
		'items_list'            => __( 'Items list', 'emmerson' ),
		'items_list_navigation' => __( 'Items list navigation', 'emmerson' ),
		'filter_items_list'     => __( 'Filter items list', 'emmerson' ),
	);
	$rewrite = array(
		'slug'                  => 'teams',
		'with_front'            => true,
		'pages'                 => true,
		'feeds'                 => true,
	);
	$args = array(
		'label'                 => __( 'Teams', 'emmerson' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'custom-fields', 'page-attributes' ),
		'taxonomies'            => array( '' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_rest' => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-portfolio',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => 'teams',
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'rewrite'               => $rewrite,
		'capability_type'       => 'page',
	);
	register_post_type( 'emmerson_team', $args );

}
add_action( 'init', 'emmerson_team', 0 );