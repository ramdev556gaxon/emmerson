<?php
/*
Plugin Name: Emmerson Functionality
Plugin URI: https://wearebauercreate.co.uk
Description: This is used to manage site functionality, do not remove.
Version: 1.0.0
Author: Bauer Create
Author URI: https://wearebauercreate.co.uk
Text Domain: emmerson
*/

include "post-types.php";