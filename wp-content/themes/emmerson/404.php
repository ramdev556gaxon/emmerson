<?php

include "header.php";

$page_title = get_field( '404_page_title','option' );
$page_tagline = get_field( '404_page_tagline' , 'option' );

echo '<div class="container">';

//echo '<h1>'.get_the_title().'</h1>';

the_content(); 
?>
 <h1><?php echo $page_title;?></h1>
 <h5><?php echo $page_tagline;?></h5>
 <a class="btn btn-white" href="<?php echo get_site_url();?>" >Go Home</a>
<?php echo '</div>';

include "footer.php";

?>