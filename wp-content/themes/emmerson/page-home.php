<?php

/*

* Template Name: Home Page

*

*

*/

include "header.php";



$video_image = get_field('video_image');

$youtube_video_id = get_field('youtube_video_id');

$testimonial_title = get_field('testimonial_title');

$testimonial_sub_title = get_field('testimonial_sub_title');

$testimonial_content = get_field('testimonial_content');

$projects_page_link = get_field('projects_page_link');

$button_text = get_field('button_text');



echo '<div class="content-area">';

?>



<div class="section text-center mb-lg-7 mb-xl-10">

    <div class="container">
     <?php while (have_posts()) :
        the_post(); ?>
        <?php

        the_content();

        ?>
      <?php endwhile;	 ?>
    </div>

</div>



<div class="section text-center">

    <div class="container">

        <?php

        $args = array(

            'post_type' => 'emmerson_portfolio',

            'posts_per_page' => 9,

        );

        $portfolio = new WP_Query($args);





        if ($portfolio->have_posts()): ?>

            <!-- <h2 class="">Our <strong>Work</strong></h2> -->

            <div class="row">

                <?php while ($portfolio->have_posts()): $portfolio->the_post();

                    $workTerms = get_the_terms($post->id, 'emmerson_portfolio_cat');

                    $image_url = get_the_post_thumbnail_url($post->ID, 'emmerson-service-feed');

                    if ($image_url) {

                        ?>

                        <div class="col-md-6 col-lg-4">

                            <div class="service-grid">

                                <div class="background-image service-grid-image" style="background-image:url(<?php echo $image_url; ?>)">
                                     &nbsp;
                                </div>

                                <div class="service-grid-content">



                                    <?php



                                    if ($workTerms) {



                                        $i = 0;

                                        $len = count($workTerms);



                                        echo '<p><small>';



                                        foreach ($workTerms as $w) {



                                            if ($i < $len - 1) {

                                                echo $w->name.', ';

                                            } else {

                                                echo $w->name;

                                            }



                                            $i++;



                                        }



                                        echo '</small></p>';



                                    }



                                    ?>



                                    <h5><a href="<?php echo get_permalink();?>"><?php echo get_the_title(); ?></a></h5>

                                    <hr style="width: 50px;">

                                </div>

                            </div>

                        </div>

                        <?php

                    }

                endwhile;

                wp_reset_postdata(); ?>

            </div>

        <?php endif; ?>

        <div class="row">

            <div class="col">

                <p><a href="/portfolio/" class="btn btn-red">View more projects</a></p>

            </div>

        </div>

    </div>

</div>



<div class="section client-section">

    <div class="container">

        <h4 class="text-center mb-lg-5 text-uppercase">Our <strong>Clients</strong></h4>

        <?php $client_logo = get_field('client_logo', 'option');

        if ($client_logo): ?>

            <div class="owl-carousel owl-theme clientlogo-carousel">

                <?php foreach ($client_logo as $logo): ?>

                    <img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>" style="max-width:100px; margin: 0 auto;"/>

                <?php endforeach; ?>

            </div>

        <?php endif; ?>

    </div>

</div>

<div class="container-fluid video-section text-center">

    <div class="row">

        <div class="video-col video-content col">

            <?php echo apply_filters('the_content', $testimonial_content); ?>

            <?php if ($testimonial_title) { ?><h5 class="mb-1"><?php echo $testimonial_title; ?></h5><?php } ?>

            <?php if ($testimonial_sub_title) { ?><h6><?php echo $testimonial_sub_title; ?></h6><?php } ?>

            <?php if ($testimonial_sub_title) { ?>

                <a class="btn btn-red" href="<?php echo $projects_page_link; ?>"><?php echo $button_text; ?></a>

            <?php } ?>

        </div>

    </div>

</div>

</div>

<?php echo '</div>';



include "footer.php";



?>
