    </div>
    <footer class="site-footer">
        <div class="container">
            <div class="row">
                <div class="col-3 col-md-2 col-lg-1">
                    <div class="site-footer-logo">
                        <a href="<?php echo esc_url(home_url('/')); ?>" rel="home">
                            <img src="http://emmerson.wearebauercreate.design/wp-content/themes/emmerson/images/footer-logo.png" alt="<?php bloginfo('name'); ?>"/>
                        </a>
                    </div>
                </div>
                <div class="col-4 col-md-4 col-lg-6">
                    <?php
                    wp_nav_menu(array(
                        'theme_location' => 'footer',
                        'menu_class' => 'footer-menu',
                    ));
                    ?>
                </div>
                <div class="col-5 col-md-4 col-lg-3 text-right">
                    <?php echo do_shortcode('[social]'); ?>
                </div>
                <div class="col-sm-6">
                    <br />
                    <br />
                    <p><small>Copyright &copy; <?php echo Date('Y'); ?> Emmerson Marketing<br />Company Registration Number: 12344567</small></p>
                </div>
                <div class="col-sm-6">
                    <br />
                    <p class="text-right"><small><a href="#">Terms & Conditions</a> | <a href="#">Privacy Policy</a></small> </p>
                </div>
            </div>
        </div>
    </footer>
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script> 
<script>
jQuery('.burger, .overlay').click(function(){
  jQuery('.burger').toggleClass('clicked');
  jQuery('.overlay').toggleClass('show');
  jQuery('nav').toggleClass('show');
  jQuery('body').toggleClass('overflow');
});
</script>
    <?php wp_footer(); ?>

  </div>
</body>
</html>