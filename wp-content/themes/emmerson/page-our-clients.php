<?php

/*

*

* Template Name: Our Clients

*

*

*/

include "header.php";

$get_in_touch_headline = get_field('get_in_touch_headline');

echo '<div class="content-area">';



//echo '<h1>'.get_the_title().'</h1>';

?>

    <div class="section text-center">

        <div class="container">
         <?php while (have_posts()) :
            the_post(); ?>
            <?php the_content(); ?>
          <?php endwhile;	 ?>
        </div>

    </div>



    <div class="section">

        <div class="container">

            <?php $client_logo = get_field('client_logo', 'option');

            if ($client_logo): ?>

                <ul class="logo-list">

                    <?php foreach ($client_logo as $logo): ?>

                        <li>

                            <img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>"/>

                        </li>

                    <?php endforeach; ?>

                </ul>

            <?php endif; ?>

            <?php if ($get_in_touch_headline) { ?>



            <?php } ?>

        </div>

    </div>



    <div class="bg-red text-white py-2 py-md-4 get-touch">

        <div class="container">

            <p class="text-uppercase mb-0"><?php echo $get_in_touch_headline; ?></p>

        </div>

    </div>

<?php echo '</div>';



include "footer.php";



?>