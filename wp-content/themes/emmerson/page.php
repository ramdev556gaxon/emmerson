<?php

include "header.php";

echo '<div class="container"><div class="row"><div class="col text-center">';

if ( have_posts() ) : while ( have_posts() ) : the_post();

the_content(); 

endwhile;

endif;

echo '</div></div></div>';

include "footer.php";

?>


