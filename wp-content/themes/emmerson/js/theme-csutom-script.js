// JavaScript Document
jQuery(document).ready(function () {
	jQuery('.clientlogo-carousel').owlCarousel({
		loop: true,
		items: 2,
		nav: false,
		dots: true,
		margin: 20,
		autoplay: 1000,
		autoplayTimeout: 2000,
		smartSpeed: 1000,
		autoplaySpeed: 1000,
		paginationSpeed: 1000,
		responsive: {
			600: {
				items: 3,
				margin: 35
			},
			991: {
				items: 4,
				margin: 55
			}
		}
	});
	jQuery(".js-video-button").modalVideo({
			youtube:{
				controls:1,
				nocookie: true
			}
		});
})