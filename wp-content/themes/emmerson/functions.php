<?php

if ( ! function_exists( 'emmerson_setup' ) ) :

	function emmerson_setup() {

		add_theme_support( 'automatic-feed-links' );

		add_theme_support( 'title-tag' );

		add_theme_support( 'post-thumbnails' );

		set_post_thumbnail_size( 1600, 600 );

		register_nav_menus(

			array(

				'header' => __( 'Header', 'emmerson' ),

				'footer' => __( 'Footer', 'emmerson' ),

				'social' => __( 'Social', 'emmerson' ),

			)

		);

		add_theme_support(

			'html5',

			array(

				'search-form',

				'comment-form',

				'comment-list',

				'gallery',

				'caption',

			)

		);

		add_theme_support( 'customize-selective-refresh-widgets' );

		add_theme_support( 'wp-block-styles' );

		add_theme_support( 'editor-styles' );

		add_theme_support( 'responsive-embeds' );

	}

endif;

add_action( 'after_setup_theme', 'emmerson_setup' );



function emmerson_content_width() {

	$GLOBALS['content_width'] = apply_filters( 'emmerson_content_width', 767 );

}

add_action( 'after_setup_theme', 'emmerson_content_width', 0 );



function emmerson_scripts() {

    wp_enqueue_script('jquery-bootstrap', get_stylesheet_directory_uri() . '/js/bootstrap.min.js', array('jquery'), '20190415', true);

    wp_enqueue_script('jquery-carousel', get_stylesheet_directory_uri() . '/js/owl.carousel.min.js', array('jquery'), '20190415', true);

    wp_enqueue_script('jquery-model-video', get_stylesheet_directory_uri() . '/js/jquery-modal-video.min.js', array('jquery'), '20190415', true);

    wp_enqueue_script('jquery-script', get_stylesheet_directory_uri() . '/js/theme-csutom-script.js', array('jquery'), '20190415', true);

    wp_enqueue_style('style', get_stylesheet_directory_uri() . '/style.css');

}

add_action( 'wp_enqueue_scripts', 'emmerson_scripts' );



if (function_exists('acf_add_options_page')) {

    acf_add_options_page(array(

        'page_title' => 'Theme Options',

        'menu_title' => 'Theme Options',

        'menu_slug' => 'ls-theme-options',

        'capability' => 'manage_options',

        'redirect' => false

    ));

    acf_add_options_sub_page(array(

        'page_title' => 'Socail Links',

        'menu_title' => 'Social Links',

        'parent_slug' => 'ls-theme-options'

    ));

	acf_add_options_sub_page(array(

        'page_title' => 'Clients logo',

        'menu_title' => 'Clients logo',

        'parent_slug' => 'ls-theme-options'

    ));


	acf_add_options_sub_page(array(

        'page_title' => '404 Page',

        'menu_title' => '404 Page',

        'parent_slug' => 'ls-theme-options'

    ));

}



function theme_body_classes($classes) {

    if (get_background_image()) {

        $classes[] = 'custom-background-image';

    }

    if (is_multi_author()) {

        $classes[] = 'group-blog';

    }

    if (is_page_template('page-home.php')) {

        $classes[] = 'home';

    }

    if (is_singular('post') || is_singular('teams')) {

        $classes[] = 'no-banner';

    }

    if (!is_singular()) {

        $classes[] = 'hfeed';

    }

    return $classes;

}

add_filter('body_class', 'theme_body_classes');



function social_fun($socialContent) {

    $facebook_link = get_field('facebook_link', 'option');

    $twitter_link = get_field('twitter_link', 'option');

    $instagram_link = get_field('instagram_link', 'option');

    $socialContent = '<ul class="social-link">';

    if ($facebook_link) {

        $socialContent .= '<li><a target="_blank" href="' . $facebook_link . '"><i class="fa fa-facebook-f"></i></a></li>';

    }

    if ($twitter_link) {

        $socialContent .= '<li><a target="_blank" href="' . $twitter_link . '"><i class="fa fa-twitter"></i></a></li>';

    }

    if ($instagram_link) {

        $socialContent .= '<li><a target="_blank" href="' . $instagram_link . '"><i class="fa fa-instagram"></i></a></li>';

    }

    $socialContent .= '</ul>';

    return $socialContent;

}

add_shortcode('social', 'social_fun', 99);

// add_filter('social', 'do_shortcode');

add_image_size( 'emmerson-service-feed', 600, 338, array( 'center', 'center' ) );