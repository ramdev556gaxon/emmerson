<?php
/*
* Template Name: Team Page
*
*
*
*/
include "header.php";

echo '<div class="content-area">';
?>

    <div class="section text-center">
        <div class="container">
         <?php while (have_posts()) :
            the_post(); ?>
            <?php the_content(); ?>
          <?php endwhile;	 ?>
        </div>
    </div>

    <div class="section">
        <div class="container">
            <?php
            $args = array(
                'post_type' => 'emmerson_team',
                'posts_per_page' => -1,
            );
            $team = new WP_Query($args);

            if ($team->have_posts()) { ?>
                <ul class="teams-list">
                    <?php $count = 0;
                    while ($team->have_posts()) : $team->the_post();
                        $imageurl = wp_get_attachment_url(get_post_thumbnail_id($post->ID), 'thumbnail');
                        $team_email_address = get_field('team_email_address');
                        ?>
                        <li class="<?php echo(++$count % 2 ? "odd" : "even"); ?>">
                            <?php if ($imageurl) { ?>
                                <div class="team-img">
                                    <img src="<?php echo $imageurl; ?>" alt=""/>
                                </div>
                            <?php } ?>
                            <div class="team-content">
                                <?php the_content(); ?>
                                <?php if ($team_email_address) { ?>
                                    <a class="btn btn-red btn-email" href="mailto:<?php echo $team_email_address; ?>">Email</a>
                                <?php } ?>
                            </div>
                        </li>
                    <?php endwhile; ?>
                </ul>
                <?php wp_reset_postdata(); ?>

            <?php } ?>
        </div>
    </div>
<?php
echo '</div>';

include "footer.php";

?>