<?php
include "header.php";
$portfolio_cats = wp_get_post_terms($post->ID, 'emmerson_portfolio_cat', array("fields" => "all"));
if ($portfolio_cats) {
    foreach ($portfolio_cats as $portfolio_cat) {
        $cat_ids[] = $portfolio_cat->term_id;
    }
} ?>
<div class="section text-center">
    <div class="container">

        <!-- Filter to be unique -->
        <div class="filters-nav-wd pb-lg-3">
            <ul class="filters-nav">
                <li><a href="#/">Design</a></li>
                <li><a href="#/">Video</a></li>
                <li><a href="#/">Marketing</a></li>
            </ul>
        </div>

        <!-- Porfolio Blocks -->
        <?php

                // If we have portfolio content
        if( have_rows('portfolio_blocks') ):

                    // While we have portfolio content
            while ( have_rows('portfolio_blocks') ) : the_row();

                $blockType = get_sub_field('portfolio_block_type');
                $colOne = get_sub_field('portfolio_column_one');
                $colTwo = get_sub_field('portfolio_column_two');
                $textBlock = get_sub_field('portfolio_text_block');
                $textBlockColOne = get_sub_field('portfolio_text_block_col_one');
                $textBlockColTwo = get_sub_field('portfolio_text_block_col_two');
                $imageBlock = get_sub_field('portfolio_image_block');
                $imageBlockColOne = get_sub_field('portfolio_image_block_col_one');
                $imageBlockColTwo = get_sub_field('portfolio_image_block_col_two');
                $youtubeBlock = get_sub_field('portfolio_youtube_video_id');
                $youtubeBlockColOne = get_sub_field('portfolio_youtube_video_id_col_one');
                $youtubeBlockColTwo = get_sub_field('portfolio_youtube_video_id_col_two');
                $vimeoBlock = get_sub_field('portfolio_vimeo_video_id');
                $vimeoBlockColOne = get_sub_field('portfolio_vimeo_video_id_col_one');
                $vimeoBlockColTwo = get_sub_field('portfolio_vimeo_video_id_col_two');
                $testimonialText = get_sub_field('portfolio_testimonial_text');
                $testimonialTextColOne = get_sub_field('portfolio_testimonial_text_col_one');
                $testimonialTextColTwo = get_sub_field('portfolio_testimonial_text_col_two');
                $testimonialAuthor = get_sub_field('portfolio_testimonial_author');
                $testimonialAuthorColOne = get_sub_field('portfolio_testimonial_author_col_one');
                $testimonialAuthorColTwo = get_sub_field('portfolio_testimonial_author_col_two');

                if ( $blockType === 'Text' ) {

                    echo '<div class="row portfolio-block align-items-center mb-4 mb-md-5"><div class="col mb-3">'.$textBlock.'</div></div>';

                } elseif ( $blockType === 'Image' ) {

                    echo '<div class="row portfolio-block align-items-center mb-4 mb-md-5"><div class="col mb-3"><img src="'.$imageBlock['url'].'" alt="'.$imageBlock['alt'].'" /></div></div>';

                } elseif ( $blockType === 'Two Column' ) {

                    echo '<div class="row portfolio-block align-items-center mb-4 mb-md-5">';

                    if ( $colOne === 'Text' ) {

                        echo '<div class="col-lg-6">'.$textBlockColOne.'</div>';

                    } elseif ( $colOne === 'Image' ) {

                        echo '<div class="col-lg-6 mb-3"><img src="'.$imageBlockColOne['url'].'" alt="'.$imageBlockColOne['alt'].'" /></div>';

                    } elseif ( $colOne === 'YouTube' ) {

                        echo '<div class="col-lg-6 mb-3"><div class="embed-responsive embed-responsive-16by9"><iframe class="embed-responsive-item" src="https://www.youtube.com/embed/'.$youtubeBlockColOne.'?rel=0"></iframe></div></div>';

                    } elseif ( $colOne === 'Vimeo' ) {

                        echo '<div class="col-lg-6 mb-3"><div class="embed-responsive embed-responsive-16by9"><iframe class="embed-responsive-item" src="https://player.vimeo.com/video/'.$vimeoBlockColOne.'"></iframe></div></div>';

                    } elseif ( $colOne === 'Testimonial' ) {

                        echo '<div class="col-lg-6"><p class="lead">'.$testimonialTextColOne.'</p><p><b>'.$testimonialAuthorColOne.'</b></p></div>';

                    }

                    if ( $colTwo === 'Text' ) {

                        echo '<div class="col-lg-6">'.$textBlockColTwo.'</div>';

                    } elseif ( $colTwo === 'Image' ) {

                        echo '<div class="col-lg-6 mb-3"><img src="'.$imageBlockColTwo['url'].'" alt="'.$imageBlockColTwo['alt'].'" /></div>';

                    } elseif ( $colTwo === 'YouTube' ) {

                        echo '<div class="col-lg-6 mb-3"><div class="embed-responsive embed-responsive-16by9"><iframe class="embed-responsive-item" src="https://www.youtube.com/embed/'.$youtubeBlockColTwo.'?rel=0"></iframe></div></div>';

                    } elseif ( $colTwo === 'Vimeo' ) {

                        echo '<div class="col-lg-6 mb-3"><div class="embed-responsive embed-responsive-16by9"><iframe class="embed-responsive-item" src="https://player.vimeo.com/video/'.$vimeoBlockColTwo.'"></iframe></div></div>';

                    } elseif ( $colTwo === 'Testimonial' ) {

                        echo '<div class="col-lg-6"><p class="lead">'.$testimonialTextColTwo.'</p><p><b>'.$testimonialAuthorColTwo.'</b></p></div>';

                    }

                    echo '</div>';

                } elseif ( $blockType === 'YouTube' ) {

                    echo '<div class="row portfolio-block align-items-center mb-4 mb-md-5"><div class="col mb-3"><div class="embed-responsive embed-responsive-16by9"><iframe class="embed-responsive-item" src="https://www.youtube.com/embed/'.$youtubeBlock.'?rel=0"></iframe></div></div></div>';

                } elseif ( $blockType === 'Vimeo' ) {

                    echo '<div class="row portfolio-block align-items-center mb-4 mb-md-5"><div class="col mb-3"><div class="embed-responsive embed-responsive-16by9"><iframe class="embed-responsive-item" src="https://player.vimeo.com/video/'.$vimeoBlock.'"></iframe></div></div></div>';

                } elseif ( $blockType === 'Testimonial' ) {

                    echo '<div class="row portfolio-block align-items-center mb-4 mb-md-5"><div class="col"><p class="lead">'.$testimonialText.'</p><p><b>'.$testimonialAuthor.'</b></p></div></div>';

                }

            endwhile;

        else :

        endif;

        ?>

        <!-- Social -->
        <?php echo do_shortcode('[social]'); ?>

    </div>
</div>
<?php
$args = array(
    'post_type' => 'emmerson_portfolio',
    'posts_per_page' => 3,
    'tax_query' => array(
        array(
            'taxonomy' => 'emmerson_portfolio_cat',
            'field' => 'term_id',
            'terms' => $cat_ids
        )
    )
);
$portfolio = new WP_Query($args);
?>
<div class="section text-center related-container">
    <div class="container">
        <?php if ($portfolio->have_posts()): ?>
            <h3 class="text-uppercase text-red mb-lg-6">Related <strong>Projects</strong></h3>
            <div class="row">
                <?php while ($portfolio->have_posts()): $portfolio->the_post();
                    $image_url = wp_get_attachment_url(get_post_thumbnail_id($post->ID), 'thumbnail');
                    if ($image_url) {
                        ?>
                        <div class="col-md-6 col-lg-4">
                            <div class="em-card">
                                <div class="background-image em-card-image" style="background-image:url(<?php echo $image_url; ?>)">
                                    &nbsp;
                                </div>
                                <div class="em-card-content">
                                    <h4 class="text-uppercase">
                                        <a href="<?php echo get_permalink(); ?>"><?php echo get_the_title(); ?></a>
                                    </h4>
                                    <a class="btn btn-sm btn-grey" href="<?php echo get_permalink(); ?>">View
                                    Now</a>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                endwhile;
                wp_reset_postdata(); ?>
            </div>
        <?php endif; ?>
    </div>
</div>
<div class="bg-red text-white py-2 py-lg-4 get-touch">
    <div class="container">
        <p class="text-uppercase mb-0">liKE WHAT YOU SEE? <a href="#/" class="btn-link"><b>get in touch?</b></a></p>
    </div>
</div>
<?php echo '</div>';
include "footer.php";
?>
