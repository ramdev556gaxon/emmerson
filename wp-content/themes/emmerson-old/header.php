<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Favicons -->
    <link rel="apple-touch-icon" sizes="180x180" href="apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="194x194" href="favicon-194x194.png">
    <link rel="icon" type="image/png" sizes="192x192" href="android-chrome-192x192.png">
    <link rel="icon" type="image/png" sizes="16x16" href="favicon-16x16.png">
    <link rel="manifest" href="site.webmanifest">
    <link rel="mask-icon" href="safari-pinned-tab.svg" color="#ff0001">
    <meta name="msapplication-TileColor" content="#ff0001">
    <meta name="msapplication-TileImage" content="/mstile-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <?php wp_head(); ?>

</head>
<body <?php body_class(); ?>>
<?php
$banner_image = get_field('banner_image');
if ($banner_image) {
    $banner_image_url = $banner_image;
} else {
    $banner_image_url = get_bloginfo('stylesheet_directory') . '/images/background.png';
}
$is_contact_button = get_field('is_contact_button');
$header_logo = get_field('header_logo', 'option');
$footer_logo = get_field('footer_logo', 'option');
if ($header_logo) {
    $logo_src = $header_logo;
} else {
    $logo_src = get_bloginfo('stylesheet_directory') . '/images/logo.png';
}
$banner_description = get_field('banner_description', get_the_ID());
$page_background_image = get_field('404_page_background_image', 'option');
?>

<div class="site"
     <?php if (is_404()) { ?>style="background-image:url('<?php echo $page_background_image; ?>')"<?php } ?>>
    <header class="site-header">
        <div class="site-header-main-wrap">
            <div class="container">
                <div class="site-header-main">
                    <?php echo do_shortcode('[social]'); ?>

                    <div class="site-logo">
                        <a href="<?php echo esc_url(home_url('/')); ?>" rel="home">
                            <img src="<?php echo $logo_src; ?>" alt="">
                        </a>
                    </div>
                    <button class="menu-toggle burger">
                        <span>&nbsp;</span>
                    </button>
                    <nav>
                        <?php
                        wp_nav_menu(array(
                            'theme_location' => 'header',
                            'menu_class' => 'primary-menu',
                        ));
                        ?>
                    </nav>
                    <div class="overlay">&nbsp;</div>
                </div>
            </div>
        </div>

        <div class="banner-wrap background-image" style="background-image:url(<?php echo $banner_image_url; ?>)">
            <div class="container">
                <div class="banner-content">
                    <?php if (is_front_page()) { ?>
                        <h1 class="mb-lg-8"><?php echo $banner_description; ?></h1>
                    <?php } else { ?>
                        <h1 class="mb-lg-8"><?php echo get_the_title(); ?></h1>
                    <?php } ?>
                    <?php if ($is_contact_button[0] == 'Yes') { ?>
                        <a class="btn btn-white" href="<?php echo get_site_url(); ?>/images/contact">Contact us</a>
                    <?php } ?>
                </div>
            </div>
        </div>
    </header>

    <div class="site-content">
	