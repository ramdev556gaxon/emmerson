<?php
/*
* Template Name: Home Page
*
*
*/
include "header.php";

$video_image = get_field('video_image');
$youtube_video_id = get_field('youtube_video_id');
$testimonial_title = get_field('testimonial_title');
$testimonial_sub_title = get_field('testimonial_sub_title');
$testimonial_content = get_field('testimonial_content');
$projects_page_link = get_field('projects_page_link');
$button_text = get_field('button_text');

echo '<div class="content-area">';

//echo '<h1>'.get_the_title().'</h1>';
?>

    <div class="section text-center mb-lg-7 mb-xl-10">
        <div class="container">
            <?php
            the_content();
            ?>
        </div>
    </div>

    <div class="section text-center">
        <div class="container">
            <?php
            $args = array(
                'post_type' => 'emmerson_portfolio',
                'posts_per_page' => 9,
            );
            $portfolio = new WP_Query($args);
            ?>
            <?php if ($portfolio->have_posts()): ?>
                <h2 class="text-dark">Our Work</h2>
                <div class="row">
                    <?php while ($portfolio->have_posts()): $portfolio->the_post();
                        $image_url = wp_get_attachment_url(get_post_thumbnail_id($post->ID), 'thumbnail');
                        if ($image_url) {
                            ?>
                            <div class="col-md-4">
                                <div class="service-grid">
                                    <img src="<?php echo $image_url; ?>" alt=""/>
                                    <div class="service-grid-content">
                                        <h5><a href="<?php echo get_permalink();?>"><?php echo get_the_title(); ?></a></h5>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                    endwhile;
                    wp_reset_postdata(); ?>
                </div>
            <?php endif; ?>
        </div>
    </div>

    <div class="section client-section">
        <div class="container">
            <h4 class="text-dark text-center mb-lg-5 text-uppercase">Our <strong>Clients</strong></h4>
            <?php $client_logo = get_field('client_logo', 'option');
            if ($client_logo): ?>
                <div class="owl-carousel owl-theme clientlogo-carousel">
                    <?php foreach ($client_logo as $logo): ?>
                        <div class="grid-equal">
                            <div class="grid-cover">
                                <div class="grid-equal-inner">
                                    <img src="<?php echo $logo['sizes']['thumbnail']; ?>"
                                         alt="<?php echo $logo['alt']; ?>"/>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
        </div>
    </div>

    <div class="video-section">
        <div class="video-row">
            <?php if ($youtube_video_id) { ?>
                <div class="video-col video-thumb">

                    <div class="fancybox-grid">
					    <a class="js-video-button" data-video-id='<?php echo $youtube_video_id; ?>'>
                            <img src="<?php echo $video_image; ?>" alt=""/>
                            <span class="icon-video">

                                    <img src="<?php echo get_bloginfo('stylesheet_directory'); ?>/images/icon-video.png"
                                         alt="<?php bloginfo('name'); ?>"/>
                                    </span>
                        </a>
                    </div>
                </div>
            <?php } ?>
            <div class="video-col video-content">
                <?php echo apply_filters('the_content', $testimonial_content); ?>
                <?php if ($testimonial_title) { ?><h5 class="mb-1"><?php echo $testimonial_title; ?></h5><?php } ?>
                <?php if ($testimonial_sub_title) { ?><h6><?php echo $testimonial_sub_title; ?></h6><?php } ?>
                <?php if ($testimonial_sub_title) { ?>
                    <a class="btn btn-red" href="<?php echo $projects_page_link; ?>"><?php echo $button_text; ?></a>
                <?php } ?>
            </div>
        </div>
    </div>
<?php echo '</div>';

include "footer.php";

?>