// JavaScript Document
jQuery(document).ready(function () {
	jQuery('.clientlogo-carousel').owlCarousel({
		loop: true,
		items: 2,
		nav: false,
		dots: true,
		margin: 20,
		
		responsive: {
			600: {
				items: 3,
				margin: 35
			},
			991: {
				items: 4,
				margin: 55
			}
		}
	});
	jQuery(".js-video-button").modalVideo({
			youtube:{
				controls:0,
				nocookie: true
			}
		});
})