<?php
/*
*
* Template Name: Portfolio Page
*
*
*/
$queried_object = get_queried_object();
$term_id = $queried_object->term_id;
include "header.php";
$trm_args = array(
    'orderby' => 'name',
    'order' => 'ASC',
    'hide_empty' => false,
);
$portfolio_categories = get_terms('emmerson_portfolio_cat', $trm_args);
echo '<div class="content-area">';

//echo '<h1>'.get_the_title().'</h1>';
?>

    <div class="section text-center">
        <div class="container">
            <?php
            the_content(); ?>
        </div>
    </div>

    <div class="section text-center">
        <div class="container">
            <div class="filters-nav-wd pb-lg-3">
                <ul class="filters-nav">
                    <?php foreach ($portfolio_categories as $category) { ?>
                        <li><a class="filter <?php if ($category->term_id == $term_id) { ?> active <?php } ?>"
                               href="<?php echo get_term_link($category->slug, 'emmerson_portfolio_cat'); ?>"><?php echo $category->name; ?></a>
                        </li>
                    <?php } ?>
                </ul>
            </div>

            <?php
            $args = array(
                'post_type' => 'emmerson_portfolio',
                'posts_per_page' => 9,
                'tax_query' => array(
                    array(
                        'taxonomy' => 'emmerson_portfolio_cat',
                        'field' => 'term_id',
                        'terms' => $term_id
                    )
                )
            );
            $portfolio = new WP_Query($args);
            ?>
            <?php if ($portfolio->have_posts()): ?>
                <div class="row">
                    <?php while ($portfolio->have_posts()): $portfolio->the_post();
                        $image_url = wp_get_attachment_url(get_post_thumbnail_id($post->ID), 'thumbnail');
                        if ($image_url) {
                            ?>
                            <div class="col-md-4">
                                <div class="service-grid">
                                    <img src="<?php echo $image_url; ?>" alt=""/>
                                    <div class="service-grid-content">
                                        <h5><a href="<?php echo get_permalink(); ?>"><?php echo get_the_title(); ?></a>
                                        </h5>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                    endwhile;
                    wp_reset_postdata(); ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
<?php echo '</div>';

include "footer.php";

?>