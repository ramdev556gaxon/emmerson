<?php

include "header.php";
$portfolio_cats = wp_get_post_terms($post->ID, 'emmerson_portfolio_cat', array("fields" => "all"));

if ($portfolio_cats) {
    foreach ($portfolio_cats as $portfolio_cat) {
        $cat_ids[] = $portfolio_cat->term_id;
    }
}
echo '<div class="content-area">';

//echo '<h1>'.get_the_title().'</h1>';
?>
    <div class="section text-center">
        <div class="container">
            <?php
            the_content(); ?>
        </div>
    </div>

    <div class="section text-center">
        <div class="container">
            <div class="filters-nav-wd pb-lg-3">
                <ul class="filters-nav">
                    <li><a href="#/">Design</a></li>
                    <li><a href="#/">Video</a></li>
                    <li><a href="#/">Marketing</a></li>
                </ul>
            </div>
            <?php if (have_rows('portfolio_repeater')): ?>

                <?php while (have_rows('portfolio_repeater')) : the_row();
                    $is_video = get_sub_field('is_video');
                    $video_image = get_sub_field('video_image');
                    $youtuve_video_id = get_sub_field('youtuve_video_id');
                    $portfolio_image = get_sub_field('portfolio_image');
                    if ($is_video[0] == 'Yes') {
                        ?>
                        <div class="mb-3 mb-lg-5">
                            <div class="fancybox-grid">
                                <a class="fancybox-youtube"
                                   href="https://youtu.be/<?php echo $youtuve_video_id; ?>?hd=1&fs=1&autoplay=1">
                                    <img class="w-100" src="<?php echo $video_image; ?>" alt=""/>
                                    <span class="icon-video">

                                    <img src="<?php echo get_bloginfo('stylesheet_directory'); ?>/images/icon-video.png"
                                         alt="<?php bloginfo('name'); ?>"/>
                                    </span>
                                </a>
                            </div>
                        </div>
                    <?php } else {
                        if ($portfolio_image) {
                            ?>
                            <div class="mb-4 mb-lg-8">
                                <img class="w-100" src="<?php echo $portfolio_image; ?>" alt=""/>
                            </div>
                        <?php }
                    } ?>
                <?php endwhile; ?>

            <?php endif; ?>

            <?php echo do_shortcode('[social]'); ?>
        </div>
    </div>

<?php
$args = array(
    'post_type' => 'emmerson_portfolio',
    'posts_per_page' => 3,
    'tax_query' => array(
        array(
            'taxonomy' => 'emmerson_portfolio_cat',
            'field' => 'term_id',
            'terms' => $cat_ids
        )
    )
);
$portfolio = new WP_Query($args);
?>

    <div class="section text-center related-container">
        <div class="container">
            <?php if ($portfolio->have_posts()): ?>
                <h3 class="text-uppercase text-red mb-lg-6">Related <strong>Projects</strong></h3>
                <div class="row">
                    <?php while ($portfolio->have_posts()): $portfolio->the_post();
                        $image_url = wp_get_attachment_url(get_post_thumbnail_id($post->ID), 'thumbnail');
                        if ($image_url) {
                            ?>
                            <div class="col-md-4">
                                <div class="em-card">
                                    <div class="em-card-thumb">
                                        <img src="<?php echo $image_url; ?>" alt=""/>
                                    </div>
                                    <div class="em-card-content">
                                        <h4 class="text-uppercase">
                                            <a href="<?php echo get_permalink(); ?>"><?php echo get_the_title(); ?></a>
                                        </h4>
                                        <a class="btn btn-sm btn-grey" href="<?php echo get_permalink(); ?>">View
                                            Now</a>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                    endwhile;
                    wp_reset_postdata(); ?>
                </div>
            <?php endif; ?>
        </div>
    </div>

    <div class="bg-red text-white py-2 py-md-4 get-touch">
        <div class="container">
            <p class="text-uppercase mb-0">liKE WHAT YOU SEE? <a href="#/" class="btn-link"><b>get in touch?</b></a></p>
        </div>
    </div>
<?php echo '</div>';

include "footer.php";

?>