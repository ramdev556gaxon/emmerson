<?php
$banner_image = get_field( 'banner_image','option' );
if( $banner_image ){
  $banner_image_url = $banner_image;
}else{
  $banner_image_url = get_bloginfo( 'stylesheet_directory').'/images/background.png';
}
is_contact_button = get_field('is_contact_button','option');
if (is_front_page()) {
   $banner_description = get_field( 'banner_description','option' );
  ?>
   <div class="banner-wrap">
	  <div class="background-image" style="background-image:url(<?php echo $banner_image_url; ?>)">
         <div class="container">
            <div class="banner-content">
                <h1><?php $banner_description; ?></h1>
				<?php echo $is_contact_button;?>
            </div>
        </div>
	  </div> 	
    </div>
<?php } elseif( is_tax() ) { ?>
	<div class="banner-wrap">
	  <div class="background-image" style="background-image:url(<?php echo $banner_image_url; ?>)">
         <div class="container">
            <div class="banner-content">
                <h1><?php get_the_title(); ?></h1>
				<?php echo $is_contact_button;?>
            </div>
        </div>
	  </div> 	
    </div>
<?php } elseif( !is_404() ) { ?>
	<div class="banner-wrap">
	  <div class="background-image" style="background-image:url(<?php echo $banner_image_url; ?>)">
         <div class="container">
            <div class="banner-content">
                <h1><?php get_the_title(); ?></h1>
				<?php echo $is_contact_button;?>
            </div>
        </div>
	  </div> 	
    </div>
<?php } ?>
