<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'emmerson' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ';K1o3vbfWK<~/i:wWzX$(OJHx|{&+^~uG0PlJd4v=V4uG-+XhjUpj+5s*9UDYw>_');
define('SECURE_AUTH_KEY',  '(Mi1JF[/w0C`K1Bk4vC)MwQ%uD=Gj;]vK+/aXxrr>&dC486b|A`)SHw:O2+^1X#_');
define('LOGGED_IN_KEY',    ',*s ~HpW?a)e]aeEB$hbQ?x:i::fqH.@8cb&xpjZqb#-0Hw0|IP8klZ0/dZvjS1%');
define('NONCE_KEY',        '1znYx&k2T9Y[xq(~Qx5Dr=v}2$)(1%sF1IJKw4jU-7;|k5X,1C:Z[H+s<v.Tir=%');
define('AUTH_SALT',        '*~@%e8%Olt@wHSR$c)-+}uWluNeqQPe+@h?Ob0q!lRngzQ4]>&Yn.OlbQ<T{m+HS');
define('SECURE_AUTH_SALT', 'kYoQrI.Xyh~?T-}C-Ty^k;sr8#,0mHd<K`MPMD.#,=#[.Wm43UJ-rg/rKQh>,z[!');
define('LOGGED_IN_SALT',   'NDD MTn#R/9j2`b*oaUwN:y.F$vyJuQkvhxTqTeV AoQi1mAAT3=U`l9a`L@zpw3');
define('NONCE_SALT',       '2+DH?c(N(5Dk-AtHcq@@I|grSKR1mrK @/bo4+14F-cbuM+4p8!}eg(:4K8$Lt0T');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
    define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
